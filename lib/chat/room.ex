defmodule Chat.Room do
  use Ecto.Schema
  import Ecto.Changeset

  alias Chat.Message

  schema "rooms" do
    field :group, :boolean, default: false
    field :owner_id, :integer
    field :partner_id, :integer, default: nil
    field :opts, :map

    timestamps()

    many_to_many(
      :message,
      Message,
      join_through: "message_room",
      on_replace: :delete
    )
  end

  @doc false
  def changeset(room, attrs) do
    room
    |> cast(attrs, [:owner_id, :opts])
    |> validate_required([:owner_id, :opts])
  end
end
