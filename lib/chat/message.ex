defmodule Chat.Message do
  use Ecto.Schema
  import Ecto.Changeset

  alias Chat.Room

  schema "messages" do
    field :content, :string
    field :author_id, :integer
    field :date, :integer, default: nil

    timestamps()

      many_to_many(
        :room,
        Room,
        join_through: "message_room",
        on_replace: :delete
      )
  end

  @doc false
  def changeset(message, attrs) do
    message
    |> cast(attrs, [:name, :message])
    |> validate_required([:name, :message])
  end

  def get_messages(limit \\ 20) do
    Chat.Repo.all(Chat.Message, limit: limit)
  end
end
