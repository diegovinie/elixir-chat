defmodule Chat.User do
  # use Agent
  use GenServer

  defstruct [:id, :name, :email, :status]

  @endpoint "http://localhost:4001/fixtures/users.json"

  def start_link(_opts) do
    users = fetch_users()
    GenServer.start_link(__MODULE__, users, name: :api_users)
  end

  def get(server, id), do: GenServer.call server, {:get, id}

  def update(server, pl), do: GenServer.call server, {:update, pl}

  def delete(server, id), do: GenServer.call server, {:delete, id}

  # implementadas
  @impl true
  def init(users), do: {:ok, users}

  @impl true
  def handle_call({:get, :all}, _sender, users), do: {:reply, users, users}
  def handle_call({:get, id}, _sender, users) do
    user = Enum.find(users, nil, fn u -> u.id === id end)
    {:reply, user, users}
  end

  @impl true
  def handle_call({:update, {id, key, val}}, _sender, users) do
    updated_users = Enum.map users, fn u ->
      if u.id !== id, do: u, else: Map.put(u, key, val)
    end
    {:reply, :ok, updated_users}
  end

  @impl true
  def handle_call({:delete, id}, _sender, users) do
    filtered_users = Enum.filter users, fn u -> u.id !== id end
    {:reply, :ok, filtered_users}
  end

  @impl true
  def handle_info(_msg, _state), do: {:noreply}

  # Privadas
  defp fetch_users() do
    response = HTTPoison.get @endpoint

    case response do
      { :error, %{:reason => error} } -> IO.inspect "error #{error}"
      { :ok, %{:body => body_json} } ->
        {:ok, body} = Poison.decode(body_json)

        Enum.map(body, &(struct_from_map(&1, as: %Chat.User{})))
    end
  end

  defp struct_from_map(a_map, as: a_struct) do
    keys = Map.keys(a_struct)
    |> Enum.filter(fn x -> x != :__struct__ end)

    processed_map =
      for key <- keys, into: %{} do
        value = Map.get(a_map, key) || Map.get(a_map, to_string(key))
        {key, value}
      end

    Map.merge(a_struct, processed_map)
  end
end
