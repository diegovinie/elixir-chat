defmodule Chat.UserRoom do
  use Ecto.Schema
  import Ecto.Changeset

  alias Chat.Message
  alias Chat.Room

  @already_exists "ALREADY_EXISTS"

  @primary_key false
  schema "message_room" do
    belongs_to(:message, Message, primary_key: true)
    belongs_to(:room, Room, primary_key: true)

    timestamps()
  end

  @required_fields ~w(message_id room_id)a
  def changeset(message_room, params \\ %{}) do
    message_room
    |> cast(params, @required_fiels)
    |> validate_required(@required_fiels)
    |> foreign_key_constraint(:project_id)
    |> foreign_key_constraint(:user_id)
    |> unique_constraint([:message, :room],
      name: :message_id_room_id_unique_index,
      message: @already_exists
    )
  end
end
