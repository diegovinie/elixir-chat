defmodule ChatWeb.RoomChannel do
  use ChatWeb, :channel

  def join("room:lobby", payload, socket) do
    if authorized?(payload) do
      send(self(), :after_join)
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  def join("room:" <> room_id, payload, socket) do
    IO.inspect room_id
    {:ok, socket}
  end

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (room:lobby).
  def handle_in("shout", payload, socket) do
    Chat.Message.changeset(%Chat.Message{}, payload) |> Chat.Repo.insert
    broadcast socket, "shout", payload
    {:noreply, socket}
  end

  def handle_in(other, _payload, socket) do
    IO.inspect other
    {:reply, socket}
  end

  # Add authorization logic here as required.
  defp authorized?(%{"id" => id}) do
    case Chat.User.get(:api_users, id) do
      nil  -> false
      user -> true
    end
  end

  def handle_info(:after_join, socket) do
    user_info = Map.from_struct(Chat.User.get :api_users, 4)
    users = Chat.User.get(:api_users, :all)
    |> Enum.map(&Map.from_struct/1)
    push(socket, "initiali", %{user: user_info, users: users})

    {:noreply, socket}
  end
end
