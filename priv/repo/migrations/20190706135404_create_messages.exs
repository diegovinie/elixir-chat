defmodule Chat.Repo.Migrations.CreateMessages do
  use Ecto.Migration

  def change do
    create table(:messages) do
      add :content, :string
      add :date, :integer, null: true
      add :author_id, :integer

      timestamps()
    end

  end
end
