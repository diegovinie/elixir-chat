defmodule Chat.Repo.Migrations.CreateRooms do
  use Ecto.Migration

  def change do
    create table(:rooms) do
      add :owner_id, :integer
      add :partner_id, :integer, null: true
      add :group, :boolean, default: false
      add :opts, :map

      timestamps()
    end

  end
end
