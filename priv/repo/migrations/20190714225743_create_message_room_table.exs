defmodule Chat.Repo.Migrations.CreateMessageRoomTable do
  use Ecto.Migration

  def change do
    create table(:message_room, primary_key: false) do
      add(:room_id, references(:rooms, on_delete: :delete_all), primary_key: true)
      add(:message_id, references(:messages, on_delete: :delete_all), primary_key: true)
      timestamps()
    end

    create(index(:message_room, [:room_id]))
    create(index(:message_room, [:message_id]))

    create(
      unique_index(:message_room, [:message_id, :room_id], name: :message_id_room_id_unique_index)
    )
  end
end
